﻿using MigraDocXML;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Drawing;

namespace TestConsole
{
	/// <summary>
	/// This is just used as a scratchpad
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			MigraDocXML_ZXing.Setup.Run();

			try
			{
				GenerateAllLessons();
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);
				if(ex.InnerException != null)
				{
					Console.WriteLine();
					Console.WriteLine("Inner Exception:");
					Console.WriteLine(ex.InnerException.Message);
					Console.WriteLine(ex.InnerException.StackTrace);
				}
				Console.ReadKey();
			}
		}


		/// <summary>
		/// Used for generating all lessons, can be used as an additional check on top of the unit tests that
		/// nothing has been broken as a result of code changes
		/// </summary>
		static void GenerateAllLessons()
		{
			var lessons = @"C:\Users\james\Documents\Code Projects\MigraDocXML\Lessons";
			GenerateLesson(lessons, "Lesson 1.0 - Setup");
			GenerateLesson(lessons, "Lesson 2.0 - Sections & Page Setup");
			GenerateLesson(lessons, "Lesson 2.1 - Paragraphs");
			GenerateLesson(lessons, "Lesson 2.2 - Tables");
			GenerateLesson(lessons, "Lesson 2.3 - Images");
			GenerateLesson(lessons, "Lesson 2.4 - TextFrames");
			GenerateLesson(lessons, "Lesson 2.5 - PointLists");
			GenerateLesson(lessons, "Lesson 2.6 - Headers & Footers");
			GenerateLesson(lessons, "Lesson 2.7 - Hyperlinks & Bookmarks");
			GenerateLesson(lessons, "Lesson 3.0 - Introduction To Scripts");
			GenerateLesson(lessons, "Lesson 3.1 - Conditionals");
			GenerateLesson(lessons, "Lesson 3.2 - Var & Set");
			GenerateLesson(lessons, "Lesson 3.3 - Arrays, Dictionaries & More Operators");
			GenerateLesson(lessons, "Lesson 3.4 - Loops & Jumps");
			GenerateLesson(lessons, "Lesson 3.5 - Injecting CSV Data", "Lesson 3.5 - Injecting CSV Data.csv");
			GenerateLesson(lessons, "Lesson 3.6 - Injecting JSON Data", "Lesson 3.6 - Injecting JSON Data.json");
			Lesson3_7_Code.Run();
			GenerateLesson(lessons, "Lesson 3.8 - Injecting XML Data", "Lesson 3.8 - Injecting XML Data - Data.xml");
			GenerateLesson(lessons, "Lesson 3.9 - Accessing The DOM");
			GenerateLesson(lessons, "Lesson 3.10 - Lambda Functions");
			GenerateLesson(lessons, "Lesson 3.11 - String Functions");
			GenerateLesson(lessons, "Lesson 3.12 - Numeric Functions");
			GenerateLesson(lessons, "Lesson 3.13 - DateTime Functions");
			GenerateLesson(lessons, "Lesson 3.14 - Enumerable Functions");
			GenerateLesson(lessons, "Lesson 3.15 - Misc Functions");
			GenerateLesson(lessons, "Lesson 3.16 - Custom Functions");
			GenerateLesson(lessons, "Lesson 4.0 - Introduction To Styles");
			GenerateLesson(lessons, "Lesson 4.1 - Style Scope & Precedence");
			GenerateLesson(lessons, "Lesson 4.2 - Advanced Style Targeting");
			GenerateLesson(lessons, "Lesson 5.0 - Resources");
			GenerateLesson(lessons, "Lesson 5.1 - Barcodes With ZXing.NET");
			GenerateLesson(lessons, "Lesson 5.2 - Charts");
			GenerateLesson(lessons, "Lesson 5.3 - Graphics");
			GenerateLesson(lessons, "Lesson 5.4 - Combining Documents");
		}


		/// <summary>
		/// Generate a single lesson pdf
		/// </summary>
		/// <param name="lessonsPath">The folder which the lesson lives in</param>
		/// <param name="lessonName">The name of the lesson, without file extension</param>
		/// <param name="dataName">Optional: The file name of the data file to be used</param>
		static void GenerateLesson(string lessonsPath, string lessonName, string dataName = null)
		{
			var designPath = Path.Combine(lessonsPath, lessonName + ".xml");
			var outputPath = Path.Combine(lessonsPath, lessonName + ".pdf");

			var reader = new PdfXmlReader(designPath);
			reader.ScriptRunner.Evaluator.Functions["GetPrimeFactors"] = EvalScriptFunctions.GetPrimeFactors;

			if (dataName == null)
				reader.Render().Save(outputPath);
			else
			{
				var dataPath = Path.Combine(lessonsPath, dataName);
				if (dataPath.EndsWith(".xml"))
					reader.RenderWithXmlFile(dataPath).Save(outputPath);
				else if (dataPath.EndsWith(".csv"))
					reader.RenderWithCsvFile(dataPath).Save(outputPath);
				else if (dataPath.EndsWith(".json"))
					reader.RenderWithJsonFile(dataPath).Save(outputPath);
			}
		}
	}
}
