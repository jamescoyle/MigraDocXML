﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class HeaderFooterTests
    {
        [Fact]
        public void PrimaryHeader()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Header IsPrimary=\"true\">" +
                            "<p>Test</p>" +
                        "</Header>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Header header = section.Children.OfType<Header>().First();
            Paragraph p = header.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Single(p.GetParagraphModel().Elements);
            
            Assert.Single(section.GetSectionModel().Headers.Primary.Elements);
            Assert.Empty(section.GetSectionModel().Headers.FirstPage.Elements);
            Assert.Empty(section.GetSectionModel().Headers.EvenPage.Elements);
            Assert.Equal(section.GetSectionModel(), header.GetHeaderModel().Section);
        }

        [Fact]
        public void FirstPageHeader()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section PageSetup.DifferentFirstPageHeaderFooter=\"true\">" +
                        "<Header IsFirstPage=\"true\">" +
                            "<p>Test</p>" +
                        "</Header>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Header header = section.Children.OfType<Header>().First();
            Paragraph p = header.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Single(p.GetParagraphModel().Elements);

            Assert.Empty(section.GetSectionModel().Headers.Primary.Elements);
            Assert.Single(section.GetSectionModel().Headers.FirstPage.Elements);
            Assert.Empty(section.GetSectionModel().Headers.EvenPage.Elements);
            Assert.Equal(section.GetSectionModel(), header.GetHeaderModel().Section);
        }

        [Fact]
        public void EvenPageHeader()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section PageSetup.OddAndEvenPagesHeaderFooter=\"true\">" +
                        "<Header IsEvenPage=\"true\">" +
                            "<p>Test</p>" +
                        "</Header>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Header header = section.Children.OfType<Header>().First();
            Paragraph p = header.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Single(p.GetParagraphModel().Elements);

            Assert.Empty(section.GetSectionModel().Headers.Primary.Elements);
            Assert.Empty(section.GetSectionModel().Headers.FirstPage.Elements);
            Assert.Single(section.GetSectionModel().Headers.EvenPage.Elements);
            Assert.Equal(section.GetSectionModel(), header.GetHeaderModel().Section);
        }

        [Fact]
        public void PrimaryFooter()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Footer IsPrimary=\"true\">" +
                            "<p>Test</p>" +
                        "</Footer>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Footer footer = section.Children.OfType<Footer>().First();
            Paragraph p = footer.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Single(p.GetParagraphModel().Elements);

            Assert.Single(section.GetSectionModel().Footers.Primary.Elements);
            Assert.Empty(section.GetSectionModel().Footers.FirstPage.Elements);
            Assert.Empty(section.GetSectionModel().Footers.EvenPage.Elements);
            Assert.Equal(section.GetSectionModel(), footer.GetFooterModel().Section);
        }

        [Fact]
        public void FirstPageFooter()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section PageSetup.DifferentFirstPageHeaderFooter=\"true\">" +
                        "<Footer IsFirstPage=\"true\">" +
                            "<p>Test</p>" +
                        "</Footer>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Footer footer = section.Children.OfType<Footer>().First();
            Paragraph p = footer.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Single(p.GetParagraphModel().Elements);

            Assert.Empty(section.GetSectionModel().Footers.Primary.Elements);
            Assert.Single(section.GetSectionModel().Footers.FirstPage.Elements);
            Assert.Empty(section.GetSectionModel().Footers.EvenPage.Elements);
            Assert.Equal(section.GetSectionModel(), footer.GetFooterModel().Section);
        }

        [Fact]
        public void EvenPageFooter()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section PageSetup.OddAndEvenPagesHeaderFooter=\"true\">" +
                        "<Footer IsEvenPage=\"true\">" +
                            "<p>Test</p>" +
                        "</Footer>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Footer footer = section.Children.OfType<Footer>().First();
            Paragraph p = footer.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Single(p.GetParagraphModel().Elements);

            Assert.Empty(section.GetSectionModel().Footers.Primary.Elements);
            Assert.Empty(section.GetSectionModel().Footers.FirstPage.Elements);
            Assert.Single(section.GetSectionModel().Footers.EvenPage.Elements);
            Assert.Equal(section.GetSectionModel(), footer.GetFooterModel().Section);
        }
    }
}
