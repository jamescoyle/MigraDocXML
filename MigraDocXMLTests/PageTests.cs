﻿using MigraDocXML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MigraDocXMLTests
{
	public class PageTests
	{
		[Fact]
		public void PageGetsSuccessfullyAddedToOutput()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Documents>" +
					"<Document Name=\"Doc1\">" +
						"<Section>" +
							"<p>Hello</p>" +
						"</Section>" +
					"</Document>" +
					"<Output>" +
						"<Page>" +
						"</Page>" +
					"</Output>" +
				"</Documents>";

			var pdfDoc = new PdfXmlReader() { DesignText = code }.Render();
			Assert.NotNull(pdfDoc);
		}

		[Fact]
		public void CanSetPageLayout()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Documents>" +
					"<Document Name=\"Doc1\">" +
						"<Section>" +
							"<p>Hello</p>" +
						"</Section>" +
					"</Document>" +
					"<Output>" +
						"<Page Width=\"5cm\" Height=\"10cm\" Orientation=\"Portrait\">" +
						"</Page>" +
					"</Output>" +
				"</Documents>";

			var pdfDoc = new PdfXmlReader() { DesignText = code }.Render();
			var page = pdfDoc.Pages[0];
			Assert.Equal(5, page.Width.Centimeter);
			Assert.Equal(10, page.Height.Centimeter);
			Assert.Equal(PdfSharp.PageOrientation.Portrait, page.Orientation);
		}
	}
}
