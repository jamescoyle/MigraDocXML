﻿using MigraDocXML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MigraDocXMLTests
{
	public class OutputTests
	{
		[Fact]
		public void OutputGetsSuccessfullyAddedToDocuments()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Documents>" +
					"<Document Name=\"Doc1\">" +
						"<Section>" +
							"<p>Hello</p>" +
						"</Section>" +
					"</Document>" +
					"<Output>" +
					"</Output>" +
				"</Documents>";

			var pdfDoc = new PdfXmlReader() { DesignText = code }.Render();
			Assert.NotNull(pdfDoc);
		}

		[Fact]
		public void OutputThrowsErrorIfDocsNotNamed()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Documents>" +
					"<Document>" +
						"<Section>" +
							"<p>Hello</p>" +
						"</Section>" +
					"</Document>" +
					"<Output>" +
					"</Output>" +
				"</Documents>";

			Assert.Throws<Exception>(() => new PdfXmlReader() { DesignText = code }.Run());
		}
	}
}
