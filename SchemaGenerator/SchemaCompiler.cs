﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public class SchemaCompiler
    {

        public int TabSize { get; set; } = 4;

        private string Tab() => new string(' ', TabSize);


        public string Compile(Schema schema)
        {
            var output = new List<string>();
            output.Add("<?xml " + string.Join(" ", schema.XmlArgs.Select(x => $"{x.Key}=\"{x.Value}\"?")) + ">");
            output.Add("<!-- This file has been auto-generated from the MigraDocXML schema generator -->");
            output.Add("<xs:schema " + string.Join(" ", schema.SchemaArgs.Select(x => $"{x.Key}=\"{x.Value}\"")) + ">");

            foreach(var type in schema.Types)
            {
                output.Add(Tab());
                output.AddRange(Compile(type).Select(x => Tab() + x));
                output.Add(Tab());
            }

            foreach(var element in schema.Elements)
            {
                output.Add(Tab());
                output.AddRange(Compile(element).Select(x => Tab() + x));
                output.Add(Tab());
            }

            output.Add("</xs:schema>");
            return string.Join(Environment.NewLine, output);
        }


        private List<string> Compile(SimpleType.ExtensionBase obj) => Compile((dynamic)obj);

        private List<string> Compile(SimpleType.PatternRestriction obj)
        {
            return new List<string>()
            {
                $"<xs:restriction base=\"{obj.BaseType}\">",
                $"{Tab()}<xs:pattern value=\"{obj.Pattern}\"/>",
                "</xs:restriction>"
            };
        }

        private List<string> Compile(SimpleType.EnumerableRestriction obj)
        {
            var output = new List<string>();
            output.Add($"<xs:restriction base=\"{obj.BaseType}\">");
            foreach (var item in obj.Values)
                output.Add($"{Tab()}<xs:enumeration value=\"{item}\"/>");
            output.Add("</xs:restriction>");
            return output;
        }

        private List<string> Compile(SimpleType.Union obj)
        {
            var output = new List<string>();
            output.Add($"<xs:union memberTypes=\"{string.Join(" ", obj.MemberTypes)}\"/>");
            return output;
        }

        private List<string> Compile(SchemaType obj) => Compile((dynamic)obj);

        private List<string> Compile(SimpleType obj)
        {
            var output = new List<string>();
            output.Add($"<xs:simpleType name=\"{obj.Name}\">");
            output.AddRange(Compile(obj.Extension).Select(x => Tab() + x));
            output.Add($"</xs:simpleType>");
            return output;
        }


        private List<string> Compile(AttributeBase obj) => Compile((dynamic)obj);

        private List<string> Compile(Attribute obj)
        {
            string outputHead = "<xs:attribute";
            if (!string.IsNullOrEmpty(obj.Name))
                outputHead += $" name=\"{obj.Name}\"";
            if (!string.IsNullOrEmpty(obj.Type))
                outputHead += $" type=\"{obj.Type}\"";
            if (!string.IsNullOrEmpty(obj.Use))
                outputHead += $" use=\"{obj.Use}\"";
            if (!string.IsNullOrEmpty(obj.Default))
                outputHead += $" default=\"{obj.Default}\"";
            outputHead += string.IsNullOrEmpty(obj.Documentation) ? "/>" : ">";

            var output = new List<string>() { outputHead };

            if (!string.IsNullOrEmpty(obj.Documentation))
            {
                output.Add(Tab() + "<xs:annotation>");
                output.Add(Tab() + Tab() + $"<xs:documentation>{obj.Documentation}</xs:documentation>");
                output.Add(Tab() + "</xs:annotation>");
                output.Add("</xs:attribute>");
            }

            return output;
        }

        private List<string> Compile(AnyAttribute obj)
        {
            string output = "<xs:anyAttribute";
            if (!string.IsNullOrEmpty(obj.ProcessContents))
                output += $" processContents=\"{obj.ProcessContents}\"";
            output += "/>";

            return new List<string>() { output };
        }


        private List<string> Compile(ElementBase obj) => Compile((dynamic)obj);

        private List<string> Compile(Element obj)
        {
            string output = $"<xs:element name=\"{obj.Name}\" type=\"{obj.Type}\"";
            if (obj.MinOccurs != null)
                output += $" minOccurs=\"{obj.MinOccurs}\"";
            if (obj.MaxOccurs != null)
                output += $" maxOccurs=\"{((obj.MaxOccurs == int.MaxValue) ? "unbounded" : obj.MaxOccurs.ToString())}\"";
            output += "/>";

            return new List<string>() { output };
        }

        private List<string> Compile(AnyElement obj)
        {
            string output = $"<xs:any";
            if (obj.MinOccurs != null)
                output += $" minOccurs=\"{obj.MinOccurs}\"";
            if (obj.MaxOccurs != null)
                output += $" maxOccurs=\"{((obj.MaxOccurs == int.MaxValue) ? "unbounded" : obj.MaxOccurs.ToString())}\"";
            output += "/>";

            return new List<string>() { output };
        }

        private List<string> Compile(SequenceElement obj)
        {
            string line1 = "<xs:sequence";
            if (obj.MinOccurs != null)
                line1 += $" minOccurs=\"{obj.MinOccurs}\"";
            if (obj.MaxOccurs != null)
                line1 += $" maxOccurs=\"{((obj.MaxOccurs == int.MaxValue) ? "unbounded" : obj.MaxOccurs.ToString())}\"";
            line1 += ">";

            List<string> output = new List<string>();
            output.Add(line1);
            output.AddRange(obj.Elements.SelectMany(x => Compile(x).Select(y => Tab() + y)));
            output.Add("</xs:sequence>");
            return output;
        }

        private List<string> Compile(ChoiceElement obj)
        {
            string line1 = "<xs:choice";
            if (obj.MinOccurs != null)
                line1 += $" minOccurs=\"{obj.MinOccurs}\"";
            if (obj.MaxOccurs != null)
                line1 += $" maxOccurs=\"{((obj.MaxOccurs == int.MaxValue) ? "unbounded" : obj.MaxOccurs.ToString())}\"";
            line1 += ">";

            List<string> output = new List<string>();
            output.Add(line1);
            output.AddRange(obj.Elements.SelectMany(x => Compile(x).Select(y => Tab() + y)));
            output.Add("</xs:choice>");
            return output;
        }

        private List<string> Compile(ComplexType obj)
        {
            List<string> output = new List<string>();
            if(obj.Elements.Count > 0)
            {
                output.Add("<xs:sequence>");
                foreach (var element in obj.Elements)
                    output.AddRange(Compile(element).Select(x => Tab() + x));
                output.Add("</xs:sequence>");
            }

            foreach (var attribute in obj.Attributes)
                output.AddRange(Compile(attribute));

            if(obj.Extension != null)
            {
                output = output.Select(x => Tab() + x).ToList();
                if (obj.Extension.IsRestriction)
                {
                    output.Insert(0, $"<xs:restriction base=\"{obj.Extension.BaseType}\">");
                    output.Add("</xs:restriction>");
                }
                else
                {
                    output.Insert(0, $"<xs:extension base=\"{obj.Extension.BaseType}\">");
                    output.Add("</xs:extension>");
                }
                output = output.Select(x => Tab() + x).ToList();
                if (obj.Extension.ComplexContent)
                {
                    output.Insert(0, "<xs:complexContent>");
                    output.Add("</xs:complexContent>");
                }
                else
                {
                    output.Insert(0, "<xs:simpleContent>");
                    output.Add("</xs:simpleContent>");
                }
            }

            output = output.Select(x => Tab() + x).ToList();

            string line1 = $"<xs:complexType name=\"{obj.Name}\"";
            if (obj.Mixed)
                line1 += " mixed=\"true\"";
            line1 += ">";

            output.Insert(0, line1);
            output.Add("</xs:complexType>");
            return output;
        }

    }
}
