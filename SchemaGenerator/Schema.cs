﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public class Schema : ComplexType
    {

        public Dictionary<string, string> XmlArgs { get; private set; } = new Dictionary<string, string>();

        public Dictionary<string, string> SchemaArgs { get; private set; } = new Dictionary<string, string>();

        public List<SchemaType> Types { get; private set; } = new List<SchemaType>();

        public Schema()
            : base(null)
        {
        }


        public ComplexType GetComplexType(string name)
        {
            return Types.OfType<ComplexType>().FirstOrDefault(x => x.Name == name);
        }

        public SimpleType GetSimpleType(string name)
        {
            return Types.OfType<SimpleType>().FirstOrDefault(x => x.Name == name);
        }

    }
}
