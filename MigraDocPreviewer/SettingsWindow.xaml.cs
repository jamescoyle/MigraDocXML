﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MigraDocPreviewer
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindowVM ViewModel { get; private set; }

        private static string _initialOpenFileDirectory;


        public SettingsWindow(SettingsWindowVM viewModel)
        {
            InitializeComponent();
            DataContext = ViewModel = viewModel;
        }


        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void LayoutFileText_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string file = ShowOpenFileDialog("XML files (*.xml)|*.xml");
            if (file != null)
                ViewModel.LayoutFile = file;
        }

        private void DataFileText_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string file = ShowOpenFileDialog("CSV files (*.csv)|*.csv|JSON files (*.json)|*.json|XML files (*.xml)|*.xml");
            if (file != null)
                ViewModel.DataFile = file;
        }

        private void PreviewFileText_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string file = ShowOpenFileDialog("PDFs (*.pdf)|*.pdf");
            if (file != null)
                ViewModel.PreviewFile = file;
        }


        private string ShowOpenFileDialog(string filter = null)
        {
            var dialog = new OpenFileDialog();
            dialog.InitialDirectory = _initialOpenFileDirectory;
            dialog.Filter = filter;
            if (dialog.ShowDialog() == true)
            {
                _initialOpenFileDirectory = System.IO.Path.GetDirectoryName(dialog.FileName);
                return dialog.FileName;
            }
            return null;
        }
    }



    public class SettingsWindowVM : INotifyPropertyChanged
    {

        private string _layoutFile;
        public string LayoutFile
        {
            get => _layoutFile;
            set
            {
                if (_layoutFile != value)
                {
                    _layoutFile = value;
                    DispatchPropertyChanged("LayoutFile");
                }
            }
        }


        private string _dataFile;
        public string DataFile
        {
            get => _dataFile;
            set
            {
                if (_dataFile != value)
                {
                    _dataFile = value;
                    DispatchPropertyChanged("DataFile");
                }
            }
        }


        private string _previewFile;
        public string PreviewFile
        {
            get => _previewFile;
            set
            {
                if (_previewFile != value)
                {
                    _previewFile = value;
                    DispatchPropertyChanged("PreviewFile");
                }
            }
        }


        public static SettingsWindowVM LoadFromSettings()
        {
            return new SettingsWindowVM()
            {
                LayoutFile = Properties.Settings.Default.LayoutFile,
                DataFile = Properties.Settings.Default.DataFile,
                PreviewFile = Properties.Settings.Default.PreviewFile
            };
        }


        public void SaveToSettings()
        {
            Properties.Settings.Default.LayoutFile = LayoutFile;
            Properties.Settings.Default.DataFile = DataFile;
            Properties.Settings.Default.PreviewFile = PreviewFile;
            Properties.Settings.Default.Save();
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void DispatchPropertyChanged(string name) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

    }
}
