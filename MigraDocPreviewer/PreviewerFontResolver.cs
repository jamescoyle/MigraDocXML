﻿using PdfSharp.Fonts;
using System.Collections.Generic;

namespace MigraDocPreviewer
{
	internal class PreviewerFontResolver : IFontResolver
	{
		private Dictionary<string, FontResolverInfo> _fonts = null;

		private string GetFontKey(string familyName, bool bold, bool italic)
		{
			return $"{familyName}%%{bold}%%{italic}";
		}

		private void AddFont(string familyName, bool bold, bool italic, FontResolverInfo resolveInfo)
		{
			_fonts[GetFontKey(familyName, bold, italic)] = resolveInfo;
		}

		private void LoadFontLookups()
		{
			_fonts = new Dictionary<string, FontResolverInfo>();
			AddFont("Calibri", false, false, new FontResolverInfo("CALIBRI.TTF", false, false));
			AddFont("Calibri", true, false, new FontResolverInfo("CALIBRIB.TTF", false, false));
			AddFont("Consolas", false, false, new FontResolverInfo("CONSOLA.TTF", false, false));
			AddFont("Courier New", false, false, new FontResolverInfo("COUR.TTF", false, false));
		}

		public FontResolverInfo ResolveTypeface(string familyName, bool bold, bool italic)
		{
			if (_fonts == null)
				LoadFontLookups();
			if (_fonts.TryGetValue(GetFontKey(familyName, bold, italic), out FontResolverInfo resolveInfo))
				return resolveInfo;
			return null;
		}

		public byte[] GetFont(string faceName)
		{
			var path = $@"C:\Windows\Fonts\{faceName}";
			return System.IO.File.ReadAllBytes(path);
		}
	}
}
