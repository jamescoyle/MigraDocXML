﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigraDocXML.Exceptions
{
    public class PropertyNotFoundException : MigraDocXMLException
    {
        public string PropertyName { get; set; }

        public string ClassName { get; set; }

        public PropertyNotFoundException()
        {
        }

        public PropertyNotFoundException(string message) : base(message)
        {
        }

        public PropertyNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
