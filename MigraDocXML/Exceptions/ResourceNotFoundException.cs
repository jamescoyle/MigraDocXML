﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigraDocXML.Exceptions
{
    public class ResourceNotFoundException : MigraDocXMLException
    {
        public string ResourceName { get; set; }


        public ResourceNotFoundException()
        {
        }

        public ResourceNotFoundException(string resourceName, string message, Exception innerException)
            : base(message, innerException)
        {
            ResourceName = resourceName;
        }

        public ResourceNotFoundException(string resourceName, string message) : base(message)
        {
            ResourceName = resourceName;
        }


        public ResourceNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ResourceNotFoundException(string message) : base(message)
        {
        }
    }
}
