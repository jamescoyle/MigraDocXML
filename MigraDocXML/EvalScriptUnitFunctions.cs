﻿using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigraDocXML
{
	public static class EvalScriptUnitFunctions
	{
		public static Unit Cm(object[] args)
		{
			if (args.Length != 1)
				throw new Exception("Cm expects 1 argument: value");
			return new Unit((double)args[0], MigraDoc.DocumentObjectModel.UnitType.Centimeter);
		}

		public static Unit Mm(object[] args)
		{
			if (args.Length != 1)
				throw new Exception("Mm expects 1 argument: value");
			return new Unit((double)args[0], MigraDoc.DocumentObjectModel.UnitType.Millimeter);
		}

		public static Unit Inches(object[] args)
		{
			if (args.Length != 1)
				throw new Exception("Inches expects 1 argument: value");
			return new Unit((double)args[0], MigraDoc.DocumentObjectModel.UnitType.Inch);
		}
	}
}
