﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigraDocXML.DOM
{
	public class Page : LogicalElement
	{
		private PdfSharp.Pdf.PdfPage _pdfSharpObject;
		public PdfSharp.Pdf.PdfPage GetPdfSharpObject() => _pdfSharpObject;

		private PdfSharp.Drawing.XGraphics _xGraphics;
		public PdfSharp.Drawing.XGraphics GetXGraphics()
		{
			if (_xGraphics == null)
				_xGraphics = PdfSharp.Drawing.XGraphics.FromPdfPage(_pdfSharpObject);
			return _xGraphics;
		}

		public Page()
		{
			NewVariable("Page", this);
			IsLogical = false;
		}

		public override void Run(Action childProcessor)
		{
			var pdfDoc = GetParentOfType<Documents>().GetResult();

			_pdfSharpObject = pdfDoc.AddPage();
			if (Width != null)
				_pdfSharpObject.Width = Width.GetXUnit();
			if (Height != null)
				_pdfSharpObject.Height = Height.GetXUnit();
			if (_orientation.HasValue)
				_pdfSharpObject.Orientation = _orientation.Value;

			childProcessor();
		}

		public Unit Width { get; set; } = new Unit(210, MigraDoc.DocumentObjectModel.UnitType.Millimeter);

		public Unit Height { get; set; } = new Unit(297, MigraDoc.DocumentObjectModel.UnitType.Millimeter);

		private PdfSharp.PageOrientation? _orientation;
		public string Orientation
		{
			get => _orientation.ToString();
			set => _orientation = Parse.Enum<PdfSharp.PageOrientation>(value);
		}
	}
}
