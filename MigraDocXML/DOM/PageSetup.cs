﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocXML.DOM
{
    public class PageSetup
    {
        private MigraDoc.DocumentObjectModel.PageSetup _model;
        public MigraDoc.DocumentObjectModel.PageSetup GetModel() => _model;

        
        public PageSetup(MigraDoc.DocumentObjectModel.PageSetup model)
        {
            _model = model ?? throw new ArgumentNullException(nameof(model));
        }



        public bool DifferentFirstPageHeaderFooter { get => _model.DifferentFirstPageHeaderFooter; set => _model.DifferentFirstPageHeaderFooter = value; }

        public bool OddAndEvenPagesHeaderFooter { get => _model.OddAndEvenPagesHeaderFooter; set => _model.OddAndEvenPagesHeaderFooter = value; }

        public bool HorizontalPageBreak { get => _model.HorizontalPageBreak; set => _model.HorizontalPageBreak = value; }

        public Unit BottomMargin { get => new Unit(_model.BottomMargin); set => _model.BottomMargin = value.GetModel(); }

        public Unit LeftMargin { get => new Unit(_model.LeftMargin); set => _model.LeftMargin = value.GetModel(); }

        public Unit RightMargin { get => new Unit(_model.RightMargin); set => _model.RightMargin = value.GetModel(); }

        public Unit TopMargin { get => new Unit(_model.TopMargin); set => _model.TopMargin = value.GetModel(); }

        public Unit HorizontalMargin
        {
            get
            {
                if (LeftMargin == null || RightMargin == null)
                    return null;
                if (LeftMargin.Equals(RightMargin))
                    return LeftMargin;
                return null;
            }
            set
            {
                LeftMargin = value;
                RightMargin = value;
            }
        }

        public Unit VerticalMargin
        {
            get
            {
                if (TopMargin == null || BottomMargin == null)
                    return null;
                if (TopMargin.Equals(BottomMargin))
                    return TopMargin;
                return null;
            }
            set
            {
                TopMargin = value;
                BottomMargin = value;
            }
        }

        public Unit Margin
        {
            get
            {
                if (HorizontalMargin == null || VerticalMargin == null)
                    return null;
                if (HorizontalMargin.Equals(VerticalMargin))
                    return HorizontalMargin;
                return null;
            }
            set
            {
                HorizontalMargin = value;
                VerticalMargin = value;
            }
        }

        public bool MirrorMargins { get => _model.MirrorMargins; set => _model.MirrorMargins = value; }

        public Unit FooterDistance { get => new Unit(_model.FooterDistance); set => _model.FooterDistance = value.GetModel(); }

        public Unit HeaderDistance { get => new Unit(_model.HeaderDistance); set => _model.HeaderDistance = value.GetModel(); }

        public string Orientation
        {
            get => _model.Orientation.ToString();
            set => _model.Orientation = Parse.Enum<MigraDoc.DocumentObjectModel.Orientation>(value);
        }

        public string PageFormat
        {
            get => _model.PageFormat.ToString();
            set
            {
                _model.PageFormat = Parse.Enum<MigraDoc.DocumentObjectModel.PageFormat>(value);

                int a0WidthMM = 841;
                int a0HeightMM = 1189;
                switch(_model.PageFormat)
                {
                    case MigraDoc.DocumentObjectModel.PageFormat.A0:
                        PageWidth = new Unit(a0WidthMM, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
                        PageHeight = new Unit(a0HeightMM, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
                        break;

					case MigraDoc.DocumentObjectModel.PageFormat.A1:
						PageWidth = new Unit(a0HeightMM / 2, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						PageHeight = new Unit(a0WidthMM, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.A2:
						PageWidth = new Unit(a0WidthMM / 2, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						PageHeight = new Unit(a0HeightMM / 2, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.A3:
						PageWidth = new Unit(a0HeightMM / 4, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						PageHeight = new Unit(a0WidthMM / 2, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.A4:
						PageWidth = new Unit(a0WidthMM / 4, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						PageHeight = new Unit(a0HeightMM / 4, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.A5:
						PageWidth = new Unit(a0HeightMM / 8, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						PageHeight = new Unit(a0WidthMM / 4, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.A6:
						PageWidth = new Unit(a0WidthMM / 8, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						PageHeight = new Unit(a0HeightMM / 8, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.B5:
						PageWidth = new Unit(182, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						PageHeight = new Unit(257, MigraDoc.DocumentObjectModel.UnitType.Millimeter);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.Ledger:
						PageWidth = new Unit(1124, MigraDoc.DocumentObjectModel.UnitType.Point);
						PageHeight = new Unit(792, MigraDoc.DocumentObjectModel.UnitType.Point);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.Legal:
						PageWidth = new Unit(612, MigraDoc.DocumentObjectModel.UnitType.Point);
						PageHeight = new Unit(1008, MigraDoc.DocumentObjectModel.UnitType.Point);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.Letter:
						PageWidth = new Unit(612, MigraDoc.DocumentObjectModel.UnitType.Point);
						PageHeight = new Unit(792, MigraDoc.DocumentObjectModel.UnitType.Point);
						break;

					case MigraDoc.DocumentObjectModel.PageFormat.P11x17:
						PageWidth = new Unit(792, MigraDoc.DocumentObjectModel.UnitType.Point);
						PageHeight = new Unit(1224, MigraDoc.DocumentObjectModel.UnitType.Point);
						break;
				}
            }
        }

        public Unit PageWidth
		{
			get => new Unit(_model.PageWidth);
			set => _model.PageWidth = value.GetModel();
		}

        public Unit PageHeight
		{
			get => new Unit(_model.PageHeight);
			set => _model.PageHeight = value.GetModel();
		}

        public string SectionStart
        {
            get => _model.SectionStart.ToString();
            set => _model.SectionStart = Parse.Enum<MigraDoc.DocumentObjectModel.BreakType>(value);
        }

        public int StartingNumber { get => _model.StartingNumber; set => _model.StartingNumber = value; }

        public Unit ContentWidth
        {
            get => PageWidth - LeftMargin - RightMargin;
            set => PageWidth = value + LeftMargin + RightMargin;
        }

        public Unit ContentHeight
        {
            get => PageHeight - TopMargin - BottomMargin;
            set => PageHeight = value + TopMargin + BottomMargin;
        }
    }
}
