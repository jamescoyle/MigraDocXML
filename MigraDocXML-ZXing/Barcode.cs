﻿using MigraDocXML.DOM;
using System;
using ZXing;
using SkiaSharp;

namespace MigraDocXML_ZXing
{
	public class Barcode : Shape
    {

        private MigraDoc.DocumentObjectModel.Shapes.Image _model;

        protected ZXing.SkiaSharp.BarcodeWriter _writer;


        public Barcode()
        {
            _model = new MigraDoc.DocumentObjectModel.Shapes.Image();
            ShapeModel = _model;
            _writer = new ZXing.SkiaSharp.BarcodeWriter();
            ParentSet += Barcode_ParentSet;
            FullyBuilt += OnFullyBuilt;
        }

        private void Barcode_ParentSet(object sender, EventArgs e)
        {
            DOMRelations.Relate(GetPresentableParent(), this);
            ApplyStyling();
        }

        public override void SetTextValue(string value)
        {
            Contents = value;
        }

        public override MigraDoc.DocumentObjectModel.DocumentObject GetModel()
        {
            return _model;
        }

        public MigraDoc.DocumentObjectModel.Shapes.Image GetImageModel()
        {
            return _model;
        }

        private static Random _random = new Random();

        private void OnFullyBuilt(object sender, EventArgs e)
        {
			if (Rotation != 0 && Rotation != 90 && Rotation != 180 && Rotation != 270)
				throw new InvalidOperationException("Error, barcode rotation angle must be zero or a multiple of 90");
            SKBitmap bitmap = _writer.Write(Contents);
			if (Rotation != 0 || FlipX || FlipY)
			{
				var transformed = new SKBitmap(
					Rotation % 180 == 0 ? bitmap.Width : bitmap.Height,
					Rotation % 180 == 0 ? bitmap.Height : bitmap.Width
				);

				using (var surface = new SKCanvas(transformed))
				{
                    var matrix = SKMatrix.CreateTranslation(-bitmap.Width / 2, -bitmap.Height / 2);
                    if (Rotation != 0)
                        matrix = matrix.PostConcat(SKMatrix.CreateRotationDegrees(Rotation));
                    if (FlipX || FlipY)
                        matrix = matrix.PostConcat(SKMatrix.CreateScale(FlipX ? -1 : 1, FlipY ? -1 : 1));
                    matrix = matrix.PostConcat(SKMatrix.CreateTranslation(transformed.Width / 2, transformed.Height / 2));
                    surface.SetMatrix(matrix);
                    surface.DrawBitmap(bitmap, 0, 0);
				}

				bitmap = transformed;
			}
            var data = bitmap.Encode(SKEncodedImageFormat.Png, 100);
            _model.Name = "base64:" + Convert.ToBase64String(data.ToArray());
        }


        public string Contents { get; set; }

        public bool LockAspectRatio { get => _model.LockAspectRatio; set => _model.LockAspectRatio = value; }

        public double Resolution { get => _model.Resolution; set => _model.Resolution = value; }

        public double ScaleHeight { get => _model.ScaleHeight; set => _model.ScaleHeight = value; }

        public double ScaleWidth { get => _model.ScaleWidth; set => _model.ScaleWidth = value; }

        public string Format
        {
            get => _writer.Format.ToString();
            set => _writer.Format = (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), value);
        }

        public bool GS1Format { get => _writer.Options.GS1Format; set => _writer.Options.GS1Format = value; }
		
        public int PixelHeight { get => _writer.Options.Height; set => _writer.Options.Height = value; }

        public int BarcodeMargin { get => _writer.Options.Margin; set => _writer.Options.Margin = value; }

        public int PixelWidth { get => _writer.Options.Width; set => _writer.Options.Width = value; }

        public bool PureBarcode { get => _writer.Options.PureBarcode; set => _writer.Options.PureBarcode = value; }

		private int _rotation = 0;
		public int Rotation
		{
			get => _rotation;
			set => _rotation = ((value % 360) + 360) % 360;
		}

		public bool FlipX { get; set; }

		public bool FlipY { get; set; }
	}
}
