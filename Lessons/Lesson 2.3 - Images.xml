<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd"
          ImagePath="Images">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 2.3 - Images</p>

        <p>In this lesson we'll be looking at some of the simple ways you can work with images in MigraDocXML. This is the image we'll be working with...</p>

        <Image Name="pdf-flat.png"/>

        <p>And the code to add it:</p>

        <p Style="Code">&lt;Image Name=&quot;pdf-flat.png&quot;/&gt;</p>

        
        <p Style="SubTitle">Image Paths</p>

        <p>Images can be referenced using both absolute and relative file paths. By default, relative file paths define a path relative to where your MigraDocXML layout file is stored. In this case, my folder structure looks something like this:</p>

        <p>
            -Lessons
            {Space(4)}-Lesson 2.3 - Images.xml
            {Space(4)}-Images
            {Space(8)}-pdf-flat.png
        </p>

        <p>So normally I'd reference my image using:</p>

        <p Style="Code">&lt;Image Name=&quot;Images\pdf-flat.png&quot;/&gt;</p>

        <p>However, to save me from repeatedly typing &quot;Images\&quot;, I've manually set the ImagePath attribute in my Document element to point to my Images folder, meaning all relative references are now defined relative to there.</p>

        <p Style="Code">&lt;Document xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
            {Space(10)}xsi:noNamespaceSchemaLocation=&quot;[My schema location]&quot;
            {Space(10)}ImagePath=&quot;Images&quot;&gt;</p>


        <PageBreak/>
        
        <p Style="SubTitle">Sizing</p>

        <p>There are 2 main options for how you want to size your images, either by absolute size, or by scaling relative to what it's saved as.</p>

        <p Style="Code" Format.SpaceAfter="2mm">&lt;Image Name=&quot;pdf-flat.png&quot; Width=&quot;3cm&quot;/&gt;</p>

        <Image Name="pdf-flat.png" Width="3cm"/>

        <p>Here we see absolute scaling, note that I only set the Width yet the Height has automatically been adjusted to preserve aspect ratio. Whereas in the next 2 examples the aspect ratio is not maintained:</p>

        <p Style="Code" Format.SpaceAfter="2mm">&lt;Image Name=&quot;pdf-flat.png&quot; Height=&quot;2cm&quot; LockAspectRatio=&quot;false&quot;/&gt;</p>

        <Image Name="pdf-flat.png" Height="2cm" LockAspectRatio="false"/>
        
        <p Style="Code" Format.SpaceBefore="5mm" Format.SpaceAfter="2mm">&lt;Image Name=&quot;pdf-flat.png&quot; Height=&quot;3cm&quot; Width=&quot;1.5cm&quot;/&gt;</p>

        <Image Name="pdf-flat.png" Height="3cm" Width="15mm"/>

        <p>Relative sizing works in much the same way, though using ScaleWidth &amp; ScaleHeight instead:</p>

        <p Style="Code" Format.SpaceAfter="2mm">&lt;Image Name=&quot;pdf-flat.png&quot; ScaleWidth=&quot;0.2&quot;/&gt;</p>

        <Image Name="pdf-flat.png" ScaleWidth="0.2"/>
        
        
        <PageBreak/>

        <p Style="SubTitle">Positioning</p>

        <Image Name="pdf-flat.png" Width="1.5cm" 
               RelativeVertical="Page" Top="Top" WrapFormat.DistanceTop="1cm" 
               RelativeHorizontal="Page" Left="Right" WrapFormat.DistanceRight="1cm"/>

        <p>By default, images are positioned following the usual document flow, however, since images are considered shapes by MigraDoc, we do have some flexibility on positioning them.</p>

        <p>The little icon placed in the top right corner of this page was added by this code:</p>

        <p Style="Code">&lt;Image Name=&quot;pdf-flat.png&quot; Width=&quot;1.5cm&quot; 
               {Space(7)}RelativeVertical=&quot;Page&quot; Top=&quot;Top&quot; WrapFormat.DistanceTop=&quot;1cm&quot;
               {Space(7)}RelativeHorizontal=&quot;Page&quot; Left=&quot;Right&quot; WrapFormat.DistanceRight=&quot;1cm&quot;/&gt;</p>

        <p>This says that the image is positioned vertically in relation to the page it's on, with its top being aligned relative to the top of the page, keeping a distance of 1cm. Horizontally, the image is also placed relative to the current page, with the image's left being aligned to the right side of the page, again keeping a distance of 1cm.</p>

        <p>There could be far more detail written on positioning, though this should at least give you a basic grasp of how to use it. For further information, you can find help in the MigraDoc forums and on stack overflow in response to people's questions when working with regular MigraDoc.</p>


        <p Style="SubTitle">Images From Memory</p>

        <p>Since the release of PDFsharp 1.50 beta 2, MigraDoc has supported adding files without specifying a filename, instead accepting a Base64 text encoding of the image into the Name property, so long as it's preceded by 'base64:'. For example:</p>

        <p Style="Code" Format.SpaceAfter="2mm">```&lt;Image Name="base64:Qk1mAAAAAAAAADYAAAAoAAAABAAAAAQAAAABABgAAAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAPL
        /APL/6KIA6KIAAPL/APL/6KIA6KIAJBztJBztTLEiTLEiJBztJBztTLEiTLEi" Width="0.5cm"/&gt;```</p>

        <Image Name="base64:Qk1mAAAAAAAAADYAAAAoAAAABAAAAAQAAAABABgAAAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAPL/APL/6KIA6KIAAPL/APL/6KIA6KIAJBztJBztTLEiTLEiJBztJBztTLEiTLEi" Width="0.5cm"/>
    </Section>
</Document>