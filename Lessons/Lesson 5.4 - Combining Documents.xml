<?xml version="1.0" encoding="utf-8"?>
<Documents xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/feature_documents/MigraDocXML/schema.xsd">
    
    <Document Name="doc1">
        <Style Target="Paragraph">
            <Setters Format.SpaceBefore="4mm" Format.SpaceAfter="4mm" Format.Font.Name="Calibri" Format.Font.Size="18"/>
        </Style>

        <Style Target="Paragraph" Name="Title">
            <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
        </Style>

        <Style Target="Paragraph" Name="SubTitle">
            <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="19"/>
        </Style>

        <Style Target="FormattedText" Name="Code">
            <Setters Font.Name="Consolas" Font.Size="14"/>
        </Style>

        <Style Target="Paragraph" Name="Code">
            <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="14" Format.Shading.Color="LightGray"/>
        </Style>

        <Section PageSetup.OddAndEvenPagesHeaderFooter="true" PageSetup.Margin="1.5cm">
            <Footer IsEvenPage="true">
                <p Format.Alignment="Right" Format.Font.Size="14"><PageField/></p>
            </Footer>

            <Footer IsPrimary="true">
                <p Format.Font.Size="14"><PageField/></p>
            </Footer>
            
            <p Format.Alignment="Center" Style="Title" Format.Font.Size="20"
               Format.SpaceAfter="5mm">MigraDocXML Lesson 5.4 - Combining Documents</p>

            <p>In the previous lesson, we looked at using the lower-level PDFSharp library to draw custom graphics on top of our designs. PDFSharp can also be used for combining the pages of one or more documents together into one single output, this can be extremely useful if for example you want to layout your document as a booklet.</p>

            <p>To achieve this all within a single design document, there's the new optional root element <FormattedText Style="Code">&lt;Documents&gt;</FormattedText>, as opposed to the <FormattedText Style="Code">&lt;Document&gt;</FormattedText> element we've been using so far.</p>

            <p>To use this new root element though, there is a change needed to the code we're using to call MigraDocXML. In lesson 1 the example given looked like this:</p>

            <p Style="Code">
                using MigraDocXML;

                ...

                string xmlFile = @"C:\Users\james\Documents\Test.xml";
                string pdfFile = @"C:\Users\james\Documents\Test.pdf";

                var pdfDoc = new PdfXmlReader(xmlFile).Run();

                if(pdfDoc == null)
                {Space(4)}return;

                var renderedDoc = pdfDoc.AddGraphics();

                renderedDoc.Save(pdfFile);

                System.Diagnostics.Process.Start(pdfFile);

            </p>

            <PageBreak/>

            <p>We can change this to just:</p>

            <p Style="Code">
                using MigraDocXML;

                ...

                string xmlFile = @"C:\Users\james\Documents\Test.xml";
                string pdfFile = @"C:\Users\james\Documents\Test.pdf";

                var renderedDoc = new PdfXmlReader(xmlFile).Render();

                if(renderedDoc == null)
                {Space(4)}return;

                renderedDoc.Save(pdfFile);

                System.Diagnostics.Process.Start(pdfFile);

            </p>

            <p>The reason for this change is that we'll now no longer be generating a single document object, but instead generating possibly several, rendering each of them, then selectively copying pages into the final rendered PDF that gets returned.</p>

            <p>It's important to note though, that even though the old <FormattedText Style="Code">pdfXmlReader.Run()</FormattedText> method doesn't work with this new layout structure, the new <FormattedText Style="Code">pdfXmlReader.Render()</FormattedText> works either way.</p>

            <PageBreak/>
            
            <p Style="SubTitle">Structure</p>

            <p>So how do we use this new format? Well essentially your design file would look something like this:</p>

            <p Style="Code">
                &lt;Documents&gt;
                {Space(2)}&lt;Document Name="doc1"&gt;
                    {Space(4)}&lt;Section&gt;
                        {Space(6)}...
                    {Space(4)}&lt;/Section&gt;
                {Space(2)}&lt;/Document&gt;
                
                {Space(2)}&lt;Document Name="doc2"&gt;
                    {Space(4)}&lt;Section&gt;
                        {Space(6)}...
                    {Space(4)}&lt;/Section&gt;
                {Space(2)}&lt;/Document&gt;
                
                {Space(2)}&lt;Output&gt;
                    {Space(4)}&lt;Var i=&quot;1&quot;&gt;
                    {Space(4)}&lt;While Test=&quot;i &lt;= doc1.Pages.Count OR i &lt;= doc2.Pages.Count&quot;&gt;
                        {Space(6)}&lt;Page Width=&quot;297mm&quot; Height=&quot;210mm&quot;&gt;
                            {Space(8)}&lt;If Test=&quot;i &lt;= doc1.Pages.Count&quot;&gt;
                                {Space(10)}&lt;Area X=&quot;0&quot; Y=&quot;0&quot; Width=&quot;```{Page.Width/2}```&quot; Height=&quot;```{Page.Height}```&quot;&gt;
                                    {Space(12)}&lt;SourcePage Document=&quot;doc1&quot; Page=&quot;```{i}```&quot;/&gt;
                                {Space(10)}&lt;/Area&gt;
                            {Space(8)}&lt;/If&gt;

                            {Space(8)}&lt;If Test=&quot;i &lt;= doc2.Pages.Count&quot;&gt;
                                {Space(10)}&lt;Area X=&quot;```{Page.Width/2}```&quot; Y=&quot;0&quot; Width=&quot;```{Page.Width/2}```&quot; Height=&quot;```{Page.Height}```&quot;&gt;
                                    {Space(12)}&lt;SourcePage Document=&quot;doc2&quot; Page=&quot;```{i}```&quot;/&gt;
                                {Space(10)}&lt;/Area&gt;
                            {Space(8)}&lt;/If&gt;
                        {Space(6)}&lt;/Page&gt;
                        
                        {Space(6)}&lt;Set i=&quot;i + 1&quot;/&gt;
                    {Space(4)}&lt;/While&gt;
                {Space(2)}&lt;/Output&gt;
                &lt;/Documents&gt;
            </p>

            <PageBreak/>

            <p>Ok, there's a fair bit to break down here. Firstly, this design is generating 2 documents: doc1 &amp; doc2. The contents of the <FormattedText Style="Code">&lt;Output&gt;</FormattedText> element then define how we use the contents of those documents to generate the PDF that will be finally returned.</p>

            <p>In this example, the generated file will contain the contents of doc1 on the left side of each page, and the contents of doc2 on the right of each page.</p>

            <p>When working with the <FormattedText Style="Code">&lt;Documents&gt;</FormattedText> element, every document that it contains must be named, to allow for them to be identified from each other later on. You'll notice that inside the <FormattedText Style="Code">&lt;Output&gt;</FormattedText> element, each document can be accessed as a variable using the name it was given.</p>

            <p>Inside the <FormattedText Style="Code">&lt;Output&gt;</FormattedText>, we've set up a while loop that compares our iteration variable to the <FormattedText Style="Code">Pages.Count</FormattedText> property of each document. If it's not greater than at least one of them, then we add a new page to our final PDF.</p>

            <p>Inside of <FormattedText Style="Code">&lt;Page&gt;</FormattedText>, we specify some <FormattedText Style="Code">&lt;Area&gt;</FormattedText> elements, and inside of each those add a <FormattedText Style="Code">&lt;SourcePage&gt;</FormattedText>. The SourcePage will grab a particular page from one of our documents and print it into our new page, filling the Area that it was added to.</p>

            <p Style="SubTitle">Finally...</p>

            <p>This is still a new feature that hopefully opens up some great new ways that MigraDocXML can be used, but has currently been left fairly barebones. There are so many things that PDFSharp can do and we're only just scratching the surface.</p>

            <p>If you find some particular use-case you have which can't currently be met with what's in here so far, then please let me know and I'll do what I can to broaden this feature.</p>
        </Section>
    </Document>
    
    <Output>
        <Var i="1"/>
        <While Test="i LT= doc1.Pages.Count">
            <Page Width="297mm" Height="210mm">
                <Area X="0cm" Y="0cm" Width="{Page.Width / 2}" Height="{Page.Height}">
                    <SourcePage Document="doc1" Page="{i}"/>
                </Area>
                <If Test="i LT doc1.Pages.Count">
                    <Area X="{Page.Width / 2}" Y="0" Width="{Page.Width / 2}" Height="{Page.Height}">
                        <SourcePage Document="doc1" Page="{i + 1}"/>
                    </Area>
                </If>
            </Page>
            <Set i="i + 2"/>
        </While>
    </Output>
</Documents>