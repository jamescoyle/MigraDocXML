<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18"
           Format.SpaceAfter="5mm">MigraDocXML Lesson 5.5 - Annotations</p>

        <p>Annotations allow you to create spaces in a document where hovering the mouse over them reveals additional information in a pop-up window. These are a feature sadly not supported in MigraDoc without making use of the underlying PdfSharp library, so to use them in MigraDocXML we'll be having to create them within the Graphics element, similarly to how we've been drawing custom graphics in lesson 5.3.</p>

        <p>Inside the Graphics element, we just have to use the TextAnnotation element, like so:</p>

        <p Style="Code">&lt;Graphics Page=&quot;1&quot;&gt;
            {Space(4)}&lt;TextAnnotation Title=&quot;This is an annotation&quot;
            {Space(20)}Contents=&quot;This is some useful text inside the annotation&quot;
            {Space(20)}Left=&quot;```{Section.PageSetup.LeftMargin}```&quot; Top=&quot;9.2cm&quot;
            {Space(20)}Icon=&quot;Help&quot; Color=&quot;Red&quot; Opacity=&quot;0.5&quot;/&gt;
            &lt;/Graphics&gt;
        </p>

        <p Format.SpaceAfter="6mm">Which produces the following result:</p>

        <Graphics Page="1">
            <TextAnnotation Title="This is an annotation"
                            Contents="This is some useful text inside the annotation"
                            Left="{Section.PageSetup.LeftMargin}" Top="9.2cm"
                            Icon="Help" Color="Red" Opacity="0.5"/>
        </Graphics>

        <p>In addition to the Left &amp; Top attributes, the TextAnnotation element does also expose Width, Height, Bottom &amp; Right attributes as well. However, the way that the annotations are rendered seems to currently ignore these properties, and just place the icon with mouse-sensitive area in the top left corner of whatever space is specified. This is something that will hopefully be resolved in an upcoming release.</p>

        <p>We can also specify the annotation's position by the area of an object already drawn to the DOM, like so:</p>

        <p Style="Code">
            &lt;p Tag=&quot;TestTag&quot;&gt;Place annotation here ->&lt;/p&gt;
            
            &lt;Graphics&gt;
            {Space(4)}&lt;TextAnnotation Title=&quot;This is another annotation&quot;
            {Space(20)}Contents=&quot;This is some useful text inside the annotation&quot;
            {Space(20)}Area=&quot;```{Section.GetTaggedAreas('TestTag')[0]}```&quot; Left=&quot;6.2cm&quot;
            {Space(20)}Icon=&quot;Note&quot; Color=&quot;Green&quot; Opacity=&quot;0.5&quot;/&gt;
            &lt;/Graphics&gt;
        </p>

        <p>Which looks like this:</p>

        <p Tag="TestTag">Place annotation here -></p>
        <Graphics>
            <TextAnnotation Title="This is another annotation"
                            Contents="This is some useful text inside the annotation"
                            Area="{Section.GetTaggedAreas('TestTag')[0]}" Left="6.2cm"
                            Icon="Note" Color="Green" Opacity="0.5"/>
        </Graphics>

        <p>Notice that in the previous example, we didn't need to set the page, because that's automatically picked up as part of setting Area. Additionally, after we'd set Area, we then set Left, this serves to override just the left part of the annotation's position, leaving everything else set by Area in tact.</p>

        <p>Unfortunately though, we did need to manually set the left measurement, because paragraphs automatically take up as much width as they can. So trying to automatically place the annotation at the end of the text with something like <FormattedText Style="Code">Left=&quot;```{Section.GetTaggedAreas('TestTag')[0].Right}```&quot;</FormattedText> would actually just place the annotation all the way over on the right side of the page.</p>
    </Section>
</Document>