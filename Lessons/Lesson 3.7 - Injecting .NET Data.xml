<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>
    
    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.7 - Injecting .NET Data</p>

        <p>In this lesson we look at injecting data into a PDF template, not from file data, but directly from objects within your program. We also look at the potential security risks that come with this and how to configure what is made accessible for your scripts to access.</p>

        <p>To actually pass an object into your template, simply amend the call to PdfXmlReader's <b>Run</b> method, passing in the object to use. The accompanying .cs file for this lesson demonstrates an example data model being passed in.</p>

        <p>We'll be aiming to recreate the same order confirmation that we did from the previous lesson using JSON data, though with some minor alterations.</p>

        <p Style="Code">
            &lt;p Format.Font.Size=&quot;16&quot; Format.Font.Underline=&quot;Single&quot;
            {Space(3)}Format.Font.Bold=&quot;true&quot; Format.Alignment=&quot;Center&quot;&gt;Order Confirmation: ```{Model.OrderNumber}```&lt;/p&gt;
            &lt;p&gt;Customer: ```{Model.Customer.Name}```&lt;/p&gt;
            &lt;p&gt;Account: ```{Model.Customer.AccountNumber}```&lt;/p&gt;
            &lt;p&gt;Date Raised: ```{Model.Date #dd MMM yyyy}```&lt;/p&gt;

            &lt;Table Borders.Color=&quot;Black&quot;&gt;
            {Space(4)}&lt;ForEach Var=&quot;colWidth&quot; In=&quot;['4.5cm', '2cm', '2cm', '2cm', '2cm', '2cm', '2cm']&quot;&gt;
            {Space(8)}&lt;Column Width=&quot;```{colWidth}```&quot;/&gt;
            {Space(4)}&lt;/ForEach&gt;

            {Space(4)}&lt;Row Heading=&quot;true&quot; Format.Font.Bold=&quot;true&quot;
            {Space(9)}C0=&quot;Product&quot; C1=&quot;Country Of Origin&quot; C2=&quot;Best Before Date&quot; C3=&quot;Batch Number&quot; 
            {Space(9)}C4=&quot;Quantity&quot; C5=&quot;Unit Price&quot; C6=&quot;Line Price&quot;/&gt;

            {Space(4)}&lt;Var totalPrice=&quot;0&quot;/&gt;
            {Space(4)}&lt;ForEach Var=&quot;orderLine&quot; In=&quot;Model.OrderLines&quot;&gt;
            {Space(8)}&lt;Var qty=&quot;orderLine.Quantity&quot; unitPrice=&quot;orderLine.Product.Price&quot;/&gt;
            {Space(8)}&lt;Var linePrice=&quot;qty * unitPrice&quot;/&gt;
            {Space(8)}&lt;Set totalPrice=&quot;totalPrice + linePrice&quot;/&gt;

            {Space(8)}&lt;Row C0=&quot;```{orderLine.Product.Name}```&quot;
            {Space(13)}C1=&quot;```{orderLine.Product.AdditionalData['CountryOfOrigin']}```&quot;
            {Space(13)}C2=&quot;```{orderLine.Product.AdditionalData['BestBefore'] #dd/MM/yy}```&quot;
            {Space(13)}C3=&quot;```{orderLine.Product.AdditionalData['BatchNum']}```&quot;
            {Space(13)}C4=&quot;```{qty}```&quot;
            {Space(13)}C5=&quot;```{unitPrice #C}```&quot;
            {Space(13)}C6=&quot;```{linePrice #C}```&quot;/&gt;
            {Space(4)}&lt;/ForEach&gt;

            {Space(4)}&lt;Row Format.Font.Bold=&quot;true&quot; C0=&quot;Total&quot; C6=&quot;```{totalPrice #C}```&quot;/&gt;
            &lt;/Table&gt;
        </p>

        <p>Produces:</p>

        <p Format.Font.Size="16" Format.Font.Underline="Single"
           Format.Font.Bold="true" Format.Alignment="Center">Order Confirmation: {Model.OrderNumber}</p>
        <p>Customer: {Model.Customer.Name}</p>
        <p>Account: {Model.Customer.AccountNumber}</p>
        <p>Date Raised: {Model.Date #dd MMM yyyy}</p>

        <Table Borders.Color="Black">
            <ForEach Var="colWidth" In="['4.5cm', '2cm', '2cm', '2cm', '2cm', '2cm', '2cm']">
                <Column Width="{colWidth}"/>
            </ForEach>

            <Row Heading="true" Format.Font.Bold="true" 
                 C0="Product" C1="Country Of Origin" C2="Best Before Date" C3="Batch Number" 
                 C4="Quantity" C5="Unit Price" C6="Line Price"/>
            
            <Var totalPrice="0"/>
            <ForEach Var="orderLine" In="Model.OrderLines">
                <Var qty="orderLine.Quantity" unitPrice="orderLine.Product.Price"/>
                <Var linePrice="qty * unitPrice"/>
                <Set totalPrice="totalPrice + linePrice"/>
                
                <Row C0="{orderLine.Product.Name}"
                     C1="{orderLine.Product.AdditionalData['CountryOfOrigin']}"
                     C2="{orderLine.Product.AdditionalData['BestBefore'] #dd/MM/yy}"
                     C3="{orderLine.Product.AdditionalData['BatchNum']}"
                     C4="{qty}"
                     C5="{unitPrice #C}"
                     C6="{linePrice #C}"/>
            </ForEach>
            
            <Row Format.Font.Bold="true" C0="Total" C6="{totalPrice #C}"/>
        </Table>

        <p>The main change from the JSON example has been the AdditionalData dictionary on product. This is to demonstrate that you can not only get object properties, but if an object also contains an indexer which takes a string parameter, that too can be accessed using the same square bracket notation used to get items from an array.</p>

        <p>One danger of using string indexers like this is that the values you can access are far more prone to change than proper compiled properties, though attempting to get an index which doesn't exist will just return null rather than throwing an error.</p>



        <p Style="SubTitle">Security</p>

        <p>In the accompanying .cs file for this lesson you can see that just before I generate my PDF, there is a line of code used to configure whether my scripts have access to functions defined on my .NET object:</p>

        <p Style="Code">
			EvalScript.Settings.AllowObjectMethodAccess = false;
		</p>

        <p>This is how things are configured by default, and how you'll most likely want to leave things 99% of the time. If you do need to access the output of some method on your objects, you can turn on <i Style="Code">AllowObjectMethodAccess</i>. This is far more risky though, since typically property getters are just used for retrieving stored data, with the occasional lazy-load. The same is not true for methods, since they are normally associated with performing some action, there is a very real chance you may inadvertantly access a method which in some way changes your model's state. Depending on your application this could range from a neat little feature all the way up to a major security risk, but as a rule, the accessing of methods is very strongly discouraged unless you absolutely have to.</p>

        <p>If you do decide not enable access to methods, then it's important to note, the script will still access them just like it does in C#: for example <i Style="Code">myObj.CalculateTax(19.99)</i>. Additionally, the method must return a value, EvalScript cannot call methods that return <i Style="Code">void</i>.</p>
        
    </Section>
</Document>