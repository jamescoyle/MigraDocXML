<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>

    <Style Target="Cell" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.Font.Size="8" Format.Font.Bold="false" Format.Font.Italic="false"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.12 - Numeric Functions</p>

        <Table Borders.Color="Black">
            <Column Width="{Section.PageSetup.ContentWidth * 0.25}" Format.Font.Bold="true"/>
            <Column Width="{Section.PageSetup.ContentWidth * 0.75}"/>

            <Style Target="Row">
                <Setters VerticalAlignment="Center"/>
            </Style>

            <Row KeepWith="1" C0="Abs" C1="Returns the non-negative version of the passed in number"/>
            <Row>
                <C0 Style="Code" MergeRight="1">```{Abs(-10)}``` >>> {Abs(-10)}
                    ```{Abs(3)}``` >>> {Abs(3)}
                </C0>
            </Row>

            <Row KeepWith="1" C0="Ceiling" C1="Rounds up to the nearest integer"/>
            <Row>
                <C0 Style="Code" MergeRight="1">```{Ceiling(3.768)}``` >>> {Ceiling(3.768)}
                    ```{Ceiling(-2.515)}``` >>> {Ceiling(-2.515)}</C0>
            </Row>

            <Row KeepWith="1" C0="Floor" C1="Rounds down to the nearest integer"/>
            <Row>
                <C0 Style="Code" MergeRight="1">```{Floor(3.768)}``` >>> {Floor(3.768)}
                ```{Floor(-2.515)}``` >>> {Floor(-2.515)}</C0>
            </Row>

            <Row KeepWith="1" C0="Max" C1="Accepts at least 2 numeric parameters, returns the largest one"/>
            <Row>
                <C0 Style="Code" MergeRight="1">```{Max(3, 7, 2, 11)}``` >>> {Max(3, 7, 2, 11)}</C0>
            </Row>

            <Row KeepWith="1" C0="Min" C1="Accepts at least 2 numeric parameters, returns the smallest one"/>
            <Row>
                <C0 Style="Code" MergeRight="1">```{Min(3, 7, 2, 11)}``` >>> {Min(3, 7, 2, 11)}</C0>
            </Row>

            <Row KeepWith="1" C0="RandomDouble" C1="Returns a random double, can optionally accept parameters that specify the inclusive minimum and exclusive maximum ends of the range, otherwise defaults to the range [0, 1)"/>
            <Row>
                <C0 Style="Code" MergeRight="1">
                    ```{RandomDouble()}``` >>> Example outputs: {Range(1, 5).Select(x => RandomDouble().Format('0.#####')).MergeToString(', ')}

                    ```{RandomDouble(504.73)}``` >>> Example outputs: {Range(1, 5).Select(x => RandomDouble(504.73).Format('0.#####')).MergeToString(', ')}

                    ```{RandomDouble(27, 30.34)}``` >>> Example outputs: {Range(1, 5).Select(x => RandomDouble(27, 30.34).Format('0.#####')).MergeToString(', ')}
                </C0>
            </Row>

            <Row KeepWith="1" C0="RandomInt" C1="Returns a random integer, can optionally accept parameters that specify the inclusive minimum and exclusive maximum ends of the range. Otherwise returns a positive integer"/>
            <Row>
                <C0 Style="Code" MergeRight="1">```{RandomInt()}``` >>> Example outputs: {Range(1, 5).Select(x => RandomInt()).MergeToString(', ')}
                
                    ```{RandomInt(10)}``` >>> Example outputs: {Range(1, 10).Select(x => RandomInt(10)).MergeToString(', ')}
                    
                    ```{RandomInt(50, 70)}``` >>> Example outputs: {Range(1, 10).Select(x => RandomInt(50, 70)).MergeToString(', ')}
                </C0>
            </Row>

            <Row KeepWith="1" C0="Round" C1="Rounds to the nearest integer"/>
            <Row>
                <C0 Style="Code" MergeRight="1">```{Round(3.768)}``` >>> {Round(3.768)}
                    ```{Round(5.326)}``` >>> {Round(5.326)}</C0>
            </Row>
        </Table>

    </Section>
</Document>