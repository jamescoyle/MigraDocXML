<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="Cell" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.Font.Size="8" Format.Font.Bold="false" Format.Font.Italic="false"/>
    </Style>


    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.16 - Custom Functions</p>

        <p>Over the previous set of lessons, I've listed the various functions which come built in to EvalScript. With any luck, these will be all you'll ever need for your PDF data manipulation needs. If that's not the case though, and you need to do something really unusual or complex, you can write your own C# functions and make them callable from MigraDocXML as if they'd been built right in.</p>

        <p>As an example, lets say I need to make a PDF that lists the prime factors of a number (eg. 660 = 2 x 2 x 3 x 5 x 11). You probably could do this purely in MigraDocXML...I'm not going to try it, and you probably shouldn't either. So instead, I write a function in C# which takes in an integer, returns an array of integers, and we'll call that from MigraDocXML instead.</p>

        <p Style="Code">```public class EvalScriptFunctions
        {
        ```{Space(4)}```public static int[] GetPrimeFactors(object[] args) 
        ```{Space(4)}```{
        ```{Space(7)}```    if(args.Length != 1 || !(args[0] is int))
        ```{Space(11)}```       throw new Exception("GetPrimeFactors expects only one argument of type int");
        ```{Space(7)}```    var output = new List&lt;int&gt;();
        ```{Space(7)}```    int a = (int)args[0];
        ```{Space(7)}```    int b = 2;
        ```{Space(7)}```    while(a >= b)
        ```{Space(7)}```    {
        ```{Space(11)}```       if(a % b == 0)
        ```{Space(11)}```       {
        ```{Space(15)}```           output.Add(b);
        ```{Space(15)}```           a /= b;
        ```{Space(11)}```       }
        ```{Space(11)}```       else
        ```{Space(15)}```           b++;
        ```{Space(7)}```    }
        ```{Space(7)}```    return output.ToArray();
        ```{Space(4)}```}
        }
        ```</p>

        <p>I'm sure this isn't the most optimised way of writing this, especially for larger numbers, but for our purposes it gets the job done. If you're curious what's happening here, <b>a</b> contains the integer that's passed into the function, <b>b</b> starts with value 2, and <b>output</b> starts empty, getting populated with the set of integers to be returned at the end of the function. We loop until <b>a</b> is smaller than <b>b</b>. On each loop, if <b>b</b> divides cleanly into <b>a</b>, then it's added to the <b>output</b>, and <b>a</b> is divided by <b>b</b>. Otherwise we increment <b>b</b> by 1. Once <b>a</b> is smaller than <b>b</b>, we should have our full list of factors.</p>

        <p>If you have experience working with C#, you'll no doubt have noticed that the function takes in a single argument which is an array of arguments. This is something common to all functions added to MigraDocXML. While this is a little annoying and means we need to do more validation of the arguments being passed in, it generally means less headaches when designing documents, since EvalScript is far looser with types than C# is.</p>

        <p>Now that we've got our function written, we just need to register the function for use with MigraDocXML. If you remember from all the way back in Lesson 1.0, we call MigraDocXML to process a document with a line like this:</p>

        <p Style="Code">var pdfDoc = new PdfXmlReader(xmlFile).Run();</p>

        <p>We're now going to modify that slightly:</p>
        
        <p Style="Code">
            var reader = new PdfXmlReader(xmlFile);
            reader.ScriptRunner.Evaluator.Functions["GetPrimeFactors"] = EvalScriptFunctions.GetPrimeFactors;
            var pdfDoc = reader.Run();
        </p>

        <p>The important line here is the 3rd one, which allows EvalScript to know whenever it finds the function name 'GetPrimeFactors', which function it should call.</p>

        <p>That's pretty much it, now we can call <FormattedText Style="Code">```{MergeToString(GetPrimeFactors(660), ', ')}```</FormattedText> and get: {MergeToString(GetPrimeFactors(660), ', ')}.</p>
    </Section>
</Document>