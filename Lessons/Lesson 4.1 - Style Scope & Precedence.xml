<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="i">
        <Setters Font.Italic="false"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 4.1 - Style Scope &amp; Prededence</p>

        <p>An important feature of styles in MigraDocXML is that they're cascading, meaning it's important to understand which styles get applied where, and the order in which they're applied.</p>

        <p>The example below shows a very rough outline of a template, where the coloured numbers down the left margin identify each point in the document that a style is created, and the corresponding coloured numbers after certain elements show where those styles are being applied, and also in what order they're applied.</p>

        <p Style="Code">
            &lt;Document&gt;
            {Space(4)}&lt;Section&gt;
            
            <i Font.Color="Red">1</i>{Space(7)}&lt;Style Target="Paragraph"/&gt;
            
            <i Font.Color="Blue">2</i>{Space(7)}&lt;Style Target="Paragraph" Name="Title"/&gt;
            
            <i Font.Color="Brown">3</i>{Space(7)}&lt;Style Target="Table"/&gt;
        
            {Space(8)}&lt;p Style="Title"&gt;hello&lt;/p&gt; <i Font.Color="Red">1</i>{Space()}<i Font.Color="Blue">2</i>
        
            {Space(8)}&lt;Table&gt; <i Font.Color="Brown">3</i>{Space()}
            
            <i Font.Color="Green">4</i>{Space(11)}&lt;Style Target="Paragraph"&gt;
        
            {Space(12)}&lt;Column/&gt;
            {Space(12)}&lt;Column/&gt;
        
            <i Font.Color="Purple">5</i>{Space(11)}&lt;Row Format.Font.Bold="true"&gt;
            {Space(16)}&lt;C0&gt;
            {Space(20)}&lt;p Style="Title"&gt;abc&lt;/p&gt; <i Font.Color="Red">1</i>{Space()}<i Font.Color="Green">4</i>{Space()}<i Font.Color="Purple">5</i>{Space()}<i Font.Color="Blue">2</i>
            
            {Space(16)}&lt;/C0&gt;
            {Space(16)}&lt;C1&gt;def&lt;/C1&gt; <i Font.Color="Red">1</i>{Space()}<i Font.Color="Green">4</i>{Space()}<i Font.Color="Purple">5</i>
            
            {Space(12)}&lt;/Row&gt;
            {Space(8)}&lt;/Table&gt;
        
            {Space(8)}&lt;p&gt;world&lt;/p&gt; <i Font.Color="Red">1</i>
        
            {Space(4)}&lt;/Section&gt;
            &lt;/Document&gt;
        
        </p>

        <p>The basic rule is that styles are applied in order from least specific to most specific. If a style is defined at the top of a Section, then everything that follows within that section is within that style's <b>scope</b>. Within that section you may have a table which defines its own style, this style's scope is limited to just the contents of that table. So the first style is less specific than the second one, meaning the first style gets applied first, allowing the second to potentially overwrite any attributes which both styles affect.</p>

        <p>Similarly, an unnamed style is less specific than a named one, so all unnamed styles get applied before named styles.</p>

        <p>In the case where there are 2 styles both defined in the same element and both named/unnamed. The style that's defined first is taken to be the less specific one of the two.</p>

        <p>So going through the above example, only styles 1 &amp; 2 can affect the 'hello' paragraph, since 3 targets tables and it's out of 4 &amp; 5's scope.</p>

        <p>The table is just affected by 3, since that's the only style in the document that targets tables.</p>

        <p>You may be wondering how there's a style being defined at 5. Whenever you set one of the 'Format.' attributes on tables, rows, columns, cells, etc., this implicitly creates a style to target all paragraph elements within it.</p>

        <p>The contents of C0 &amp; C1 in the row are both paragraphs, and so get affected by the paragraph styles. These are both within the scope of all paragraph styles (1, 2, 4 &amp; 5). First all of the unnamed styles get applied, so 1 first, being least specific, then 4 since it targets just the table contents, then 5 since it targets just the row contents. Since the paragraph in C0 also names a style to use, style 2 is then applied only after all unnamed styles have been applied.</p>

        
        
        <PageBreak/>
        <p Style="SubTitle">Combining Named Styles</p>

        <p>The ability for elements to optionally name styles which should apply to them does give a lot of flexibility, though if you were limited to just one named style per element would potentially result in lots of variations of very similar styles for ever so slightly different uses. For this reason, elements can use multiple named styles.</p>
    
        <p Style="Code">
            &lt;Style Target="Paragraph" Name="Emphasis"&gt;
            {Space(4)}&lt;Setters Format.Font.Italic="true" Format.Font.Underline="Single"/&gt;
            &lt;/Style&gt;
            
            &lt;Style Target="Paragraph" Name="Urgent"&gt;
            {Space(4)}&lt;Setters Format.Font.Bold="true" Format.Font.Color="Red"/&gt;
            &lt;/Style&gt;
            
            &lt;p Style="Emphasis"&gt;This is important&lt;/p&gt;
            
            &lt;p Style="Urgent"&gt;This is urgent&lt;/p&gt;
            
            &lt;p Style="Urgent Emphasis"&gt;This is urgent &amp; important&lt;/p&gt;
        
        </p>
    
        <Style Target="Paragraph" Name="Emphasis">
            <Setters Format.Font.Italic="true" Format.Font.Underline="Single"/>
        </Style>
    
        <Style Target="Paragraph" Name="Urgent">
            <Setters Format.Font.Bold="true" Format.Font.Color="Red"/>
        </Style>
    
        <p Style="Emphasis">This is important</p>
        
        <p Style="Urgent">This is urgent</p>
        
        <p Style="Urgent Emphasis">This is urgent &amp; important</p>
        
        <p>The order in which named styles are applied is not based on the order they're listed in the element, but follows the same rules as defined above, with the widest scoped styles being applied first.</p>
        
    </Section>
</Document>