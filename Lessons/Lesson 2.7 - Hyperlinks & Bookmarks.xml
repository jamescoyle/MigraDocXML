<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>
    
    <Section PageSetup.PageHeight="18cm">
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 2.7 - Hyperlinks &amp; Bookmarks<Bookmark>TopOfPage</Bookmark></p>

        <p>In this lesson we cover how to create links both within your document, and to resources outside of it.</p>

        <list Type="NumberList1">
            <p><a Font.Underline="Single" Font.Color="Blue" Type="Bookmark" Name="Bookmarks">Bookmarks</a></p>
            <p><a Font.Underline="Single" Font.Color="Blue" Type="Bookmark" Name="Hyperlinks">Hyperlinks</a></p>
        </list>
        
        
        
        <PageBreak/>

        <p Style="SubTitle">Bookmarks<Bookmark>Bookmarks</Bookmark></p>

        <p>A bookmark is simply a way to tag a part of your document so that it can be pointed to by some other element. Adding a bookmark to a paragraph element means that you can create a hyperlink which, when clicked, takes the user to that paragraph.</p>

        <p>Bookmarks are always added to paragraph elements, and done like so:</p>

        <p Style="Code">&lt;p&gt;Some paragraph text&lt;Bookmark&gt;MyFirstBookmark&lt;/Bookmark&gt;&lt;/p&gt;</p>

        <p><a Font.Underline="Single" Font.Color="Blue" Type="Bookmark" Name="TopOfPage">Back To Top</a></p>
        
        

        <PageBreak/>

        <p Style="SubTitle">Hyperlinks<Bookmark>Hyperlinks</Bookmark></p>

        <p>A hyperlink is added as a child element of a paragraph. In it you have the usual text formatting options, plus the <b>Type</b> and <b>Name</b> attributes, which are really the important ones for setting up your hyperlink.</p>

        <p>Since we've just shown how to add bookmarks, lets first cover how to link to them by demonstrating how to create the &quot;Back To Top&quot; link at the bottom of this page.</p>

        <p Style="Code">&lt;p&gt;&lt;a Font.Underline=&quot;Single&quot; Font.Color=&quot;Blue&quot; Type=&quot;Bookmark&quot; Name=&quot;TopOfPage&quot;&gt;Back To Top&lt;/a&gt;&lt;/p&gt;</p>

        <p>The <b>a</b> element is what defines my hyperlink, this is following the convention already established by HTML, though if you prefer, you can define hyperlinks with the <b>Hyperlink</b> element.</p>

        <p>I added a bookmark to the title at the top of this document called &quot;TopOfPage&quot;, so I set my hyperlink Type=&quot;Bookmark&quot; and Name=&quot;TopOfPage&quot;.</p>

        <p>Worth noting is that hyperlinks don't automatically get formatted to the blue underlined text we all know and love. You will need to manually style your links to make them stand out from plain text.</p>

        <p>To link to webpages, you do exactly the same process, except setting <b>Name</b> to the page URL, and <b>Type=&quot;Web&quot;</b>. For example:</p>

        <p Style="Code">&lt;p&gt;&lt;a Font.Underline=&quot;Single&quot; Font.Color=&quot;Blue&quot; Name=&quot;https://gitlab.com/jamescoyle/MigraDocXML&quot; Type="Web"&gt;MigraDocXML Home Page&lt;/a&gt;&lt;/p&gt;</p>

        <p>Produces:</p>

        <p><a Font.Underline="Single" Font.Color="Blue" Name="https://gitlab.com/jamescoyle/MigraDocXML" Type="Web">MigraDocXML Home Page</a></p>

        <p>If you want to display the URL as the link text, you can make things even easier, using:</p>

        <p Style="Code">&lt;p&gt;&lt;a Font.Underline=&quot;Single&quot; Font.Color=&quot;Blue&quot;&gt;https://gitlab.com/jamescoyle/MigraDocXML&lt;/a&gt;&lt;/p&gt;</p>

        <p><a Font.Underline="Single" Font.Color="Blue" Type="Bookmark" Name="TopOfPage">Back To Top</a></p>

    </Section>
</Document>